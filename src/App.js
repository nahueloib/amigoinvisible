import "./App.css";
import { useState } from "react";
import matches from "./family_sort.json";

const options = [
    "pedro",
    "sandra",
    "nahu",
    "vicky",
    "fiori",
    "agustin",
    "mariela",
];

function App() {
    const [selected, setSelected] = useState(null);

    return (
        <div className="overflow-y-hidden bg-gray-200 flex flex-col absolute inset-0">
            <div className="bg-green-500 flex items-center justify-left flex-row px-6 py-6">
                <p
                    className="text-4xl text-white text-left ml-4"
                    style={{ fontFamily: "Bree Serif" }}
                >
                    Papá Noel invisible
                    <br></br>
                    <span className="text-5xl">2023</span>
                </p>
                <img
                    alt="santa"
                    className="w-40"
                    src={require("./assets/santa.png")}
                ></img>
            </div>
            <div className="w-full bg-red-500 flex items-center justify-center py-2">
                <p
                    className="text-white text-2xl"
                    style={{ fontFamily: "Bree Serif" }}
                >
                    ¿Quién sos?
                </p>
            </div>
            <div className="overflow-y-auto bg-gray-200 py-6 flex-1 shrink-0">
                {selected ? (
                    <div className="bg-white w-3/4 m-auto px-4 py-2 rounded-md">
                        <p
                            className="text-xl my-4 text-center"
                            style={{ fontFamily: "Bree Serif" }}
                        >
                            <span className="capitalize">{selected}</span>, este
                            año te toco regalarle a...
                        </p>
                        <Card
                            name={matches[selected]}
                            setSelected={setSelected}
                        />
                        <p
                            className="text-xl my-4 text-center"
                            style={{ fontFamily: "Bree Serif" }}
                        >
                            Jo Jo Jo, ahora si ¡A regalar!
                        </p>
                    </div>
                ) : (
                    <div className="flex flex-row flex-wrap gap-4 items-center justify-center px-4">
                        {options.map((name) => (
                            <Card name={name} setSelected={setSelected} />
                        ))}
                    </div>
                )}
            </div>
        </div>
    );
}

const Card = ({ name, setSelected }) => {
    return (
        <div
            className="flex gap-5 rounded-full bg-white items-center p-2 w-3/4 bg-white"
            onClick={() => {
                setSelected(name);
            }}
        >
            <img
                className="w-20 h-20 rounded-full"
                alt={name}
                src={require(`./assets/${name}.jpg`)}
            ></img>
            <p
                className="text-3xl text-black capitalize"
                style={{ fontFamily: "Bree Serif" }}
            >
                {name}
            </p>
        </div>
    );
};

export default App;
